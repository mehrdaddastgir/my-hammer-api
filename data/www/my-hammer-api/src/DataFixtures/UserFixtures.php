<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail(substr(str_shuffle("qwertyuiopasdfghjklzxcvbnm"),0, 10) . '@gmail.com');
            $user->setName(substr(str_shuffle("qwertyuiopasdfghjklzxcvbnm"),0, rand(4, 12)));
            $manager->persist($user);
        }

        $manager->flush();
    }
}
