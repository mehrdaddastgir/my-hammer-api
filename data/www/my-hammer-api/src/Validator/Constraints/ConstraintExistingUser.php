<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintExistingUser extends Constraint
{
    public $message = 'The user does not exist in the database.';
}