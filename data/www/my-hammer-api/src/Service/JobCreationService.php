<?php

namespace App\Service;

use App\Entity\Job;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobCreationService
{

    /** @var EntityManager */
    private $entityManager;

    /** @var ValidatorInterface */
    private $validator;

    private $container;

    public function __construct(
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        ContainerInterface $container
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->container = $container;
    }

    public function createJob(Request $request): JsonResponse
    {
        try {
            $job = $this->fillJobEntity($request);
            $errors = $this->validator->validate($job);
            if (count($errors) > 0) {
                return new JsonResponse([
                    'type' => 'validation_error',
                    'title' => 'There was validation error(s)',
                    'errors' => $this->generateErrorMessages($errors)
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $this->entityManager->persist($job);
            $this->entityManager->flush();
            /** @var Serializer $serializer */
            $serializer = $this->container->get('serializer');
            return new JsonResponse(json_decode($serializer->serialize($job, 'json')), Response::HTTP_OK);
        } catch (ORMException $exception) {
            return new JsonResponse([
                'type' => 'server_error',
                'title' => 'Something went wrong',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function generateErrorMessages(ConstraintViolationListInterface $errors): array
    {
        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[$error->getPropertyPath()] = $error->getMessage();
        }
        return $errorMessages;
    }

    private function fillJobEntity(Request $request): Job
    {
        $job = new Job();
        $job->setTitle($request->request->get('title'));
        $job->setDescription($request->request->get('description'));
        $job->setZipcode($request->request->get('zipcode'));
        $job->setServiceId($request->request->get('service_id'));
        $job->setUserId($request->request->get('user_id'));
        $job->setCity($request->request->get('city'));
        $job->setCompletionDate(
            $request->request->get('completion_date')
                ? new DateTime($request->request->get('completion_date'))
                : null
        );
        $job->setCreatedAt(new DateTime());
        return $job;
    }
}