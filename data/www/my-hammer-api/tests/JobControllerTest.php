<?php

namespace App\Tests;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JobControllerTest extends WebTestCase
{

    public function testGetRequestIsNotAllowed()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_GET, '/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $client->getResponse()->getStatusCode());
    }

    public function testPatchRequestIsNotAllowed()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_PATCH, '/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $client->getResponse()->getStatusCode());
    }

    public function testPutRequestIsNotAllowed()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_PUT, '/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $client->getResponse()->getStatusCode());
    }

    public function testDeleteRequestIsNotAllowed()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_DELETE, '/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $client->getResponse()->getStatusCode());
    }

    public function testPostRequestIsAllowed()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_POST, '/jobs', []);
        $this->assertNotEquals(Response::HTTP_METHOD_NOT_ALLOWED, $client->getResponse()->getStatusCode());
    }

    public function testJobCreateSuccessfully()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_POST, '/jobs', [
            'title' => 'This is a test title',
            'description' => 'This is a description',
            'city' => 'berlin',
            'zipcode' => '20095',
            'completion_date' => (new DateTime("+2 day"))->format('Y-m-d H:i:s'),
            'user_id' => 1,
            'service_id' => 1
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testValidationErrors()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(Request::METHOD_POST, '/jobs', [
            'title' => '1234',
            'description' => '1234',
            'city' => '',
            'zipcode' => '01000',
            'completion_date' => (new DateTime("-2 day"))->format('Y-m-d H:i:s'),
            'user_id' => 50,
            'service_id' => 20
        ]);

        $errors = (json_decode($client->getResponse()->getContent(), false))->errors;
        $expectedErrors = [
            'title' => 'The title must be at least 5 characters long',
            'description' => 'This value is too short. It should have 5 characters or more.',
            'city' => 'This value should not be blank.',
            'zipcode' => 'The zipcode is not the correct German format.',
            'user_id' => 'The user does not exist in the database.',
            'service_id' => 'The service does not exist in the database.'
        ];
        $this->assertArraySubset($expectedErrors, (array)$errors);
    }
}
