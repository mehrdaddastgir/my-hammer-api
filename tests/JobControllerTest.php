<?php

namespace App\Tests;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JobControllerTest extends WebTestCase
{

    protected $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    public function testGetRequestIsNotAllowed()
    {
        $this->client->xmlHttpRequest(Request::METHOD_GET, '/API/v1/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $this->client->getResponse()->getStatusCode());
    }

    public function testPatchRequestIsNotAllowed()
    {
        $this->client->xmlHttpRequest(Request::METHOD_PATCH, '/API/v1/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $this->client->getResponse()->getStatusCode());
    }

    public function testPutRequestIsNotAllowed()
    {
        $this->client->xmlHttpRequest(Request::METHOD_PUT, '/API/v1/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $this->client->getResponse()->getStatusCode());
    }

    public function testDeleteRequestIsNotAllowed()
    {
        $this->client->xmlHttpRequest(Request::METHOD_DELETE, '/API/v1/jobs', []);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $this->client->getResponse()->getStatusCode());
    }

    public function testPostRequestIsAllowed()
    {
        $this->client->xmlHttpRequest(Request::METHOD_POST, '/API/v1/jobs', []);
        $this->assertNotEquals(Response::HTTP_METHOD_NOT_ALLOWED, $this->client->getResponse()->getStatusCode());
    }

    public function testJobCreateSuccessfully()
    {
        $this->client->xmlHttpRequest(Request::METHOD_POST, '/API/v1/jobs', [
            'title' => 'This is a test title',
            'description' => 'This is a description',
            'city' => 'berlin',
            'zipcode' => '20095',
            'completion_date' => (new DateTime("+2 day"))->format('Y-m-d H:i:s'),
            'user_id' => 1,
            'service_id' => 1
        ]);

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testValidationErrors()
    {
        $this->client->xmlHttpRequest(Request::METHOD_POST, '/API/v1/jobs', [
            'title' => '1234',
            'description' => '1234',
            'city' => '',
            'zipcode' => '01000',
            'completion_date' => (new DateTime("-2 day"))->format('Y-m-d H:i:s'),
            'user_id' => 50,
            'service_id' => 20
        ]);

        $errors = (json_decode($this->client->getResponse()->getContent(), false))->errors;
        $expectedErrors = [
            'title' => 'The title must be at least 5 characters long',
            'description' => 'This value is too short. It should have 5 characters or more.',
            'city' => 'This value should not be blank.',
            'zipcode' => 'The zipcode is not the correct German format.',
            'user_id' => 'The user does not exist in the database.',
            'service_id' => 'The service does not exist in the database.'
        ];
        $this->assertArraySubset($expectedErrors, (array)$errors);
    }
}
