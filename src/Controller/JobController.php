<?php

namespace App\Controller;

use App\Entity\Job;
use App\Service\JobCreationService;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class JobController extends FOSRestController
{

    /** @var JobCreationService */
    private $jobService;

    /**
     * JobController constructor.
     * @param JobCreationService $jobService
     */
    public function __construct(JobCreationService $jobService)
    {
        $this->jobService = $jobService;
    }

    /**
     * @Rest\Post("/jobs", name="jobs")
     */
    public function index(Request $request): JsonResponse
    {
        return $this->jobService->createJob($request);
    }
}
