<?php

namespace App\DataFixtures;

use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ServiceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $service = new Service();
            $service->setName(substr(str_shuffle("qwertyuiopasdfghjklzxcvbnm"),0, 10));
            $manager->persist($service);
        }

        $manager->flush();
    }
}
