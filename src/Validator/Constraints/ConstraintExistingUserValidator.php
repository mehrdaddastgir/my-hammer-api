<?php

namespace App\Validator\Constraints;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConstraintExistingUserValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ConstraintExistingUserValidator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        // I'm against yoda style but this is from the documentation :)
        if (null === $value || '' === $value) {
            return;
        }

        if (!$this->entityManager->getRepository(User::class)->find($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ integer }}', $value)
                ->addViolation();
        }
    }
}