<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintGermanZipCode extends Constraint
{
    public $message = 'The zipcode is not the correct German format.';
}