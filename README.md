# MyHammer Code challenge.

As requested, this code challenge was done using devilbox and symfony v4.1. This is not a perfect solution as this mini project was my first attempt at the symfony framework
### Installation
This project depends on devilbox, to install first clone devilbox
```sh
$ git clone https://github.com/cytopia/devilbox
```

After cloning, you need to create and edit the `.env` file and update the following values:
To create simply:
```sh
$ cp env-example .env
```

* `NEW_UID` should be set to your host machine user id. You can run the following command to get it.
```sh
$ id -u
```
* `NEW_GID` should be set to your host machine group id. You can run the following command to get it.
```sh
$ id -g
```
You then need to clone this repository and create a symlink (if not existing)
```sh
$ cd <devilbox folder>/data/www
$ git clone https://mehrdaddastgir@bitbucket.org/mehrdaddastgir/my-hammer-api.git
$ mkdir htdocs
$ cd htdocs
$ ln -s my-hammer-api/public/index.php index.php
```
You need to update your `/etc/hosts` file and insert `127.0.0.1 my-hammer-api.loc`

### Starting and Entering the devilbox
Make sure you are running the docker client at this stage.
Once the devilbox containers are setup, you need to run:
```bash
$ cd <devilbox folder>
$ docker-compose up
```
alternatively you can pass the process to the background by passing in the `-d` flag at the end.
once your containers are ready, you can enter the box by the following command:
```bash
$ ./shell.sh
```
After entering your container, go to the project folder and run composer install
```sh
$ cd /shared/httpd/my-hammer-api
$ composer install
```
### Database Setup (From the container) 
Create the database based on the config by running the following console command:
```bash
$ php bin/console doctrine:database:create
```
Please note, you can modify the database configuration from your `.env` file.
Run the following command to migrate the database:
```bash
$ php bin/console doctrine:migrations:migrate
```
 We require a few records in our User and Service tables, we can run the below fixture:
```bash
$ php bin/console doctrine:fixtures:load
```
## API Documentation

### Headers
  
**``Content-Type``** ``application/json`` ``required`` Set the content type on the expected response to JSON.
### Endpoints

| &nbsp;                            | &nbsp;            | &nbsp;               |
| --------------------------------- | ----------------- | -------------------- |
| **endpoint**                      | **method**            | **URI**               |
| create job | ``POST`` | ``/jobs`` |

### Responses

| Code    | Description     | Response Body |
|---------|-----------------|---------------|
| ``422`` | Unprocessable Entity (there are validation errors) | JSON with errors |
| ``500`` | Internal Server Error (unexpected system behavior) | JSON with message |
| ``200`` | OK (resource created) | JSON with Job Entity|

### Example success response
```json
{
	"id": 3,
	"title": "Craftsman needed",
	"description": "I need a craftsman to change my bathroom tiles",
	"zipcode": "13355",
	"city": "Berlin",
	"completionDate": "2018-10-20T00:00:00+02:00",
	"serviceId": 3,
	"userId": 2,
	"createdAt": "2018-10-04T02:14:38+02:00"
}
```

### Example validation error response
```json
{
	"type": "validation_error",
	"title": "There was validation error(s)",
	"errors": {
		"title": "The title must be at least 5 characters long",
		"description": "This value is too short. It should have 5 characters or more.",
		"zipcode": "The zipcode is not the correct German format.",
		"completion_date": "This value should be greater than Oct 4, 2018, 12:00 AM.",
		"user_id": "The user does not exist in the database.",
		"city": "This value should not be blank."
	}
}
```
### Properties:

+ ``title`` **string** (`min: 5` - `max: 50`)
+ ``description`` **string** (`min: 5` - `max: 1023`)
+ ``zipcode`` **string** (``"/^0[1-9]\d\d(?<!0100)0|0[1-9]\d\d[1-9]|[1-9]\d{3}[0-8]|[1-9]\d{3}(?<!9999)9$/"``)
+ ``user_id`` **integer**
+ ``service_id`` **integer**
+ ``completion_date`` **string** (``Y-m-d H:i:s``)
+ ``city`` **string**
